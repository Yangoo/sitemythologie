<!DOCTYPE html>
<html>
<?php include("connexion.php");?>
<head>
 
  <meta charset="UTF-8">
 
  <link rel="stylesheet" href="CSS/bootstrap.min.css">
  <link rel="stylesheet" href="CSS/style.css">
  <title>MythologieGrecque</title>
 
</head>



 
<body>
  <a name="haut"></a>
  <header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light ">
      <div class="container-fluid">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon">
 
          </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link " aria-current="page" href="index.php">Accueil</a>
            </li>
            <li class="nav-item" id="menu">
              <a class="nav-link" href="Dieuxfin.php">Les Dieux</a>
            </li>
            <li class="nav-item" id="menu">
              <a class="nav-link" href="Heros.php">Les Héros</a>
            </li>
            <li class="nav-item" id="menu">
              <a class="nav-link" href="creaturesleg.php">Les Créatures Légendaires</a>
            </li>
            <li class="nav-item" id="menu">
              <a class="nav-link" href="totLieux.php">Les Lieux </a>
            </li>
            <li class="nav-item" id="menu">
              <a class="nav-link" href="#">Les Mythes</a>
            </li>
              <li class="nav-item" id="menu">
              <a class="nav-link connexion" href="#">Se connecter</a>
              </li>
 
          </ul>
        </div>
      </div>
    </nav>
  </header>
